package main

import (
    "bufio"
    "fmt"
    "io/ioutil"
    "os"
    "gopkg.in/yaml.v2"
)

var settings_file = "./settings.yml"

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func main() {
    data, err := ioutil.ReadFile(settings_file)
    check(err)

    // load settings
    settings := make(map[interface{}]interface{})
    err = yaml.Unmarshal([]byte(data), &settings)
    fmt.Printf("settings are:\n%s\n\n", settings)
    check(err)

    for {
        fmt.Println("enter your command ")
        command, _ := bufio.NewReader(os.Stdin).ReadString('\n')

        fmt.Println("you entered", command)
    }

    fmt.Println("\n\npress enter to exit...")
    bufio.NewReader(os.Stdin).ReadString('\n')
}
