#!/usr/bin/env bash

# ignore fnf errors (2)
rm go.mod 2>/dev/null
go mod init go-text-adventure
